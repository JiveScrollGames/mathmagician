﻿using UnityEngine;

namespace Assets.Scripts.ScriptableObjects.Objects
{
    [CreateAssetMenu]
    public class LoggedInUser : ScriptableObject
    {
        private bool IsLoggedIn = false;
        private string Username;
        private int Game1HighScore;

        public void OnLogin(string user, int highScore)
        {
            IsLoggedIn = true;
            Username = user;
            Game1HighScore = highScore;
        }

        public void OnLogout()
        {
            IsLoggedIn = false;
            Username = null;
            Game1HighScore = 0;
        }

        public int GetHighScore()
        {
            return Game1HighScore;
        }

        public void SetNewHighScore(int highScore)
        {
            Game1HighScore = highScore;
        }

        public string GetLoggedInUser()
        {
            return Username;
        }
    }
}
