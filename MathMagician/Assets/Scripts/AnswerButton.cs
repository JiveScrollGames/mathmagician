﻿using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour
{
    private Text answerText;
    private int answerValue;
    private bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        answerText = GetComponentInChildren<Text>();
    }

    public void SetAnswer(int value)
    {
        answerValue = value;
        answerText.text = answerValue.ToString();
    }
    
    public int GetAnswer()
    {
        return answerValue;
    }

    public void SetActive(bool active)
    {
        active = isActive;
    }

    public bool GetActive()
    {
        return isActive;
    }
}
