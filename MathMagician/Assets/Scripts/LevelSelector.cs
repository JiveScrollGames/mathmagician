﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    private bool loadingStarted = false;

    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void LoadLevelDelayed(string name)
    {
        if (!loadingStarted)
        {
            StartCoroutine(LoadSceneDelayed(name));
        }
    }

    IEnumerator LoadSceneDelayed(string sceneName)
    {
        loadingStarted = true;
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ReloadLevelDelayed()
    {
        var currentScene = SceneManager.GetActiveScene().name;

        if (!loadingStarted)
        {
            StartCoroutine(LoadSceneDelayed(currentScene));
        }
    }

    public void QuitRequest()
    {
        Application.Quit();
    }

    public void QuitRequestDelayed()
    {
        if (!loadingStarted)
        {
            StartCoroutine(QuitDelayed());
        }
        Application.Quit();
    }

    IEnumerator QuitDelayed()
    {
        loadingStarted = true;
        yield return new WaitForSeconds(1);
        Application.Quit();
    }
}
