﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class QuizMaster : MonoBehaviour
{
    public int minNumber;
    public int maxNumber;
    public int answerDivergence;

    private int sumElement1;
    private int sumElement2;
    private int answerValue;
    private int[] answerValues;

    private int solutionValue;
    private RemovedElement removedElement;

    public Text questionElement1;
    public Text questionElement2;
    public Text answerElement;

    public Text[] answers;

    public UnityEvent readyToAnswer;
    public UnityEvent answerRight;
    public UnityEvent answerWrong;

    private enum RemovedElement
    {
        element1 = 1,
        element2 = 2,
        answer = 3
    }

    public void Start()
    {
        CreateQuestion();
    }

    public void CreateQuestion()
    {
        GenerateQuestion();
        removedElement = ChooseElementToRemove();
        PopulateQuestionUI();
        GenerateAnswers();
        PopulateAnswersUI();

        readyToAnswer.Invoke();
    }

    public void GenerateQuestion()
    {
        sumElement1 = Random.Range(minNumber, maxNumber);
        sumElement2 = Random.Range(minNumber, maxNumber);
        answerValue = sumElement1 + sumElement2;
    }

    private RemovedElement ChooseElementToRemove()
    {
        return (RemovedElement)Random.Range(1, 4);
    }

    private void PopulateQuestionUI()
    {
        questionElement1.text = "";
        questionElement2.text = "";
        answerElement.text = "";

        if (removedElement == RemovedElement.element1)
        {
            questionElement2.text = sumElement2.ToString();
            answerElement.text = answerValue.ToString();

        }
        else if(removedElement == RemovedElement.element2)
        {
            questionElement1.text = sumElement1.ToString();
            answerElement.text = answerValue.ToString();
        }
        else
        {
            questionElement1.text = sumElement1.ToString();
            questionElement2.text = sumElement2.ToString();
        }
    }

    public void PopulateAnswersUI()
    {
        for(int i = 0; i < answers.Length; i++)
        {
            answers[i].text = answerValues[i].ToString();
        }
    }

    public void GenerateAnswers()
    {
        int positionForAnswer = Random.Range(0, answers.Length);
        answerValues = new int[answers.Length];

        solutionValue = GetSolution();
        
        for (int i = 0; i < answers.Length; i++)
        {
            if(i == positionForAnswer)
            {
                answerValues[i] = solutionValue;
            }
            else
            {
                answerValues[i] = GenerateAnswer();
            }

        }
    }

    private int GetSolution()
    {
        if (removedElement == RemovedElement.element1)
        {
            return sumElement1;

        }
        else if (removedElement == RemovedElement.element2)
        {
            return sumElement2;
        }
        else
        {
            return answerValue;
        }
    }

    private int GenerateAnswer()
    {
        int generatedAnswer = 0;

        bool validAnswer = false;

        while(!validAnswer)
        {
            generatedAnswer = Random.Range(solutionValue - answerDivergence, solutionValue + answerDivergence);

            validAnswer = true;
            foreach(int answerValue in answerValues)
            {
                if(generatedAnswer == answerValue || generatedAnswer == solutionValue)
                {
                    validAnswer = false;
                }
            }
        }
        return generatedAnswer;
    }

    public void CheckAnswer(Text submittedAnswerText)
    {
        if (submittedAnswerText.text == solutionValue.ToString())
        {
            answerRight.Invoke();
            Debug.Log("Correct Answer");
        }
        else
        {
            answerWrong.Invoke();
            Debug.Log("Wrong Answer");
        }

        CreateQuestion();
    }
}
