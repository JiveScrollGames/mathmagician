﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreText : MonoBehaviour
{
    public IntVariable highScoreValue;

    public void GetHighScore()
    {
        GetComponent<Text>().text = highScoreValue.Value.ToString();
    }
}
