﻿using System.Collections.Generic;

namespace Assets.Scripts
{
    [System.Serializable]
    public class SaveData
    {
        public int musicVolume = 10;
        public int sfxVolume = 10;
        public int highScore = 0;
        public List<UserData> userList = new List<UserData>();
    }

    public class UserData
    {
        public string userName;
        public int highScore = 0;
    }
}
