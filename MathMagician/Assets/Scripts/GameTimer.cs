﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    public int startSeconds;
    private float timeRemaining;

    private bool timerPaused = false;

    public UnityEvent onTimeOver;
    public Text timerDisplayText;

    // Start is called before the first frame update
    void Start()
    {
        InitializeTimer();        
    }

    private void InitializeTimer()
    {
        timeRemaining = startSeconds;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (timeRemaining > 0 && !timerPaused)
        {
            timeRemaining -= Time.deltaTime;
        }

        if (timerDisplayText != null)
        {
            UpdateTimerDisplay();
        }

        if (timeRemaining <= 0)
        {
            onTimeOver.Invoke();
        }
    }

    private void UpdateTimerDisplay()
    {
        timerDisplayText.text = ((int)timeRemaining).ToString();
    }

    public void PauseTimer()
    {
        timerPaused = true;
    }

    public void UnPauseTimer()
    {
        timerPaused = false;
    }

    public void AddTime(int timeToAdd)
    {
        timeRemaining += timeToAdd;
    }

    public void RemoveTime(int timetoRemove)
    {
        timeRemaining -= timetoRemove;

        if(timeRemaining < 0)
        {
            timeRemaining = 0;
        }
    }

    public void HandleCorrectAnswer()
    {

    }

    public void HandleWrongAnswer()
    {

    }
}
