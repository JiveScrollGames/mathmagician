﻿using UnityEngine;
using UnityEngine.UI;

public class GameScoring : MonoBehaviour
{

    public IntVariable currentGameScore;
    public Text[] scoreDisplayText;

    // Start is called before the first frame update
    void Start()
    {
        ResetScore();
        UpdateScoreDisplay();
    }
    
    public void IncrementScore()
    {
        currentGameScore.Value++;
        UpdateScoreDisplay();
    }

    public void ResetScore()
    {
        currentGameScore.Value = 0;
    }

    private void UpdateScoreDisplay()
    {
        if (scoreDisplayText != null)
        {
            foreach (Text scoreText in scoreDisplayText)
            {
                scoreText.text = currentGameScore.Value.ToString();
            }
        }
    }
}
