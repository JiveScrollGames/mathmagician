﻿using UnityEngine;

public class HighScoreSetter : MonoBehaviour
{
    public GameEvent settingsChangedEvent;
    public GameEvent saveDataEvent;
    public IntVariable currentHighScore;

    public void SetHighScore(IntVariable currentScore)
    {
        if (currentScore.Value > currentHighScore.Value)
        {
            currentHighScore.Value = currentScore.Value;
            settingsChangedEvent.Raise();
            saveDataEvent.Raise();
        }
    }
}
